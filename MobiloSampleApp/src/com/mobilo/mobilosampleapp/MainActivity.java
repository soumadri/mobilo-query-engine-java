package com.mobilo.mobilosampleapp;

import java.io.IOException;

import org.antlr.runtime.RecognitionException;

import com.mobilo.mobilosampleapp.search.MobiloManager;
import com.mobiloindexer.domain.SearchResult;
import com.mobiloqueryengine.core.MobiloQueryEngine;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	
	EditText mEdit;
	MobiloQueryEngine queryEngine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		//Instantiate the search engine
		final String path=Environment.getExternalStorageDirectory().getAbsolutePath();
		try {
			queryEngine = MobiloManager.getMobilo(path);
		} catch (IOException e) {
			Log.e("MQE",e.getMessage());			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

	public void search(View view) throws IOException, ClassNotFoundException, RecognitionException {
		
		mEdit   = (EditText)findViewById(R.id.editText1);
		String q = mEdit.getText().toString();
		
		Log.i("MQE","Querying: "+q);
		
		SearchResult result = queryEngine.search(q);	//"Mrs. Bennet"
		
		if(null==result)
			Toast.makeText(getApplicationContext(), "No results found", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(getApplicationContext(), "Matches: "+result.getMatchCount(), Toast.LENGTH_LONG).show();
		
		/*Log.i("PATH",path);
		Thread t=new Thread(new Runnable() {
			
			@Override
			public void run()
			{
				File f=new File(path);
				String[] list=f.list();
				for(String name:list)
				{
					Log.e("DIRNAMES",name);
				}
				
			}
		});
		t.start();
		
		Toast.makeText(getApplicationContext(), "Path: "+path, Toast.LENGTH_LONG).show();*/
		
	}
}
