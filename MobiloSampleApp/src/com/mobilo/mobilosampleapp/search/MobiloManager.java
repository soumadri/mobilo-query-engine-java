package com.mobilo.mobilosampleapp.search;

import java.io.IOException;

import com.mobiloqueryengine.core.MobiloQueryEngine;

public class MobiloManager {
	static MobiloQueryEngine mobiloQE;
	
	private MobiloManager(){}
	
	public static MobiloQueryEngine getMobilo(String path) throws IOException{
		if(null == mobiloQE)
			return new MobiloQueryEngine(path);
		else
			return mobiloQE;
	}
}
