package com.mobiloindexer.domain;

import java.util.Comparator;

public class DocumentRefComparator implements Comparator<DocumentReference> {

	@Override
	public int compare(DocumentReference o1, DocumentReference o2) {				
		if (o1.getDocid() > o2.getDocid()) {
			return 1;
		} else if (o1.getDocid() == o2.getDocid()) {
			return 0;
		} else {
			return -1;
		}
	}
}
