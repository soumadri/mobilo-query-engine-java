package com.mobiloindexer.domain;
import java.io.Serializable;


public class Hit implements Serializable, Comparable<Hit> {
	int startposition;
	int endposition;
	public int getStartposition() {
		return startposition;
	}
	public void setStartposition(int startposition) {
		this.startposition = startposition;
	}
	public int getEndposition() {
		return endposition;
	}
	public void setEndposition(int endposition) {
		this.endposition = endposition;
	}
	
	@Override
	public int compareTo(Hit o) {
		int startPosition = o.getStartposition();
		
		if (this.startposition < startPosition) {
			return 1;
		} else if (this.startposition == startPosition) {
			return 0;
		} else {
			return -1;
		}		
	}

}
