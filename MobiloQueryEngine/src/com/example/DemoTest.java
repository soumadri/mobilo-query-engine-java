package com.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;

import org.antlr.runtime.RecognitionException;
import org.apache.log4j.Logger;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.Hit;
import com.mobiloindexer.domain.SearchResult;
import com.mobiloqueryengine.core.MobiloQueryEngine;


public class DemoTest {
	private final static Logger LOGGER = Logger.getLogger(DemoTest.class);
	/**
	 * @param args
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException 
	 * @throws RecognitionException 
	 */
	
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, RecognitionException {
		LOGGER.info("Starting query engine");
		MobiloQueryEngine queryEngine =  new MobiloQueryEngine("/home/soumadriroy/my-projects/mobilo-samples/sample1-index/tmp0.5245320315217821/");
		SearchResult result = queryEngine.search("solid and world");//"law or rules");		
		
		if(null==result)
			LOGGER.info("No results found");
		else {
			LOGGER.info("Matches: "+result.getMatchCount());
			LOGGER.info(serializeResult(result.getDocumentReferences()));			
		}
		LOGGER.info("Results retrived");		
	}
	
	
	
	public static String serializeResult(LinkedList<DocumentReference> demoList){
		Iterator<DocumentReference> iterator = demoList.iterator();
		StringBuilder resultString = new StringBuilder();
		
		while (iterator.hasNext()) {
			
			DocumentReference documentRefrence= iterator.next();
			
			resultString.append("\n[DocID: " + documentRefrence.getDocid()+", DocName: " + documentRefrence.getDocName() + ", Number of hits: "+documentRefrence.getHitcount() +", Hits: ");
			LinkedList<Hit> list = documentRefrence.getHitlist();
			Collections.sort(list);
			
			for(Hit hit:list){
				resultString.append("{Start: "+hit.getStartposition()+", End: "+hit.getEndposition()+"}");
				
				//LOGGER.info("MATCH: " + readFile(hit.getStartposition(), hit.getEndposition(), documentRefrence.getDocName()));
			}
			resultString.append("]\n");
		}
		
		return resultString.toString();
	}
	
	public static String readFile(int start, int end, String fileName){				
		File file = new File("D:\\codelib\\MobiloData\\input1\\"+fileName+".html");
	    FileInputStream fis;
	    byte[] data;
		try {
			fis = new FileInputStream(file);
			data = new byte[(int)file.length()];
		    fis.read(data);
		    fis.close();
		    String s = new String(data, "UTF-8");
		    return s.substring(start, end);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	    return "";	    
	}

}
