package com.mobiloqueryengine.core;
// $ANTLR 3.5.1 C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g 2014-06-18 15:36:50

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class MobiloQueryGrammerLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__8=8;
	public static final int T__9=9;
	public static final int AND=4;
	public static final int NAME=5;
	public static final int OR=6;
	public static final int SPACE=7;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public MobiloQueryGrammerLexer() {} 
	public MobiloQueryGrammerLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public MobiloQueryGrammerLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g"; }

	// $ANTLR start "T__8"
	public final void mT__8() throws RecognitionException {
		try {
			int _type = T__8;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:2:6: ( '(' )
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:2:8: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__8"

	// $ANTLR start "T__9"
	public final void mT__9() throws RecognitionException {
		try {
			int _type = T__9;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:3:6: ( ')' )
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:3:8: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__9"

	// $ANTLR start "OR"
	public final void mOR() throws RecognitionException {
		try {
			int _type = OR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:10:4: ( 'OR' | 'or' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='O') ) {
				alt1=1;
			}
			else if ( (LA1_0=='o') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:10:4: 'OR'
					{
					match("OR"); 

					}
					break;
				case 2 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:10:11: 'or'
					{
					match("or"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OR"

	// $ANTLR start "AND"
	public final void mAND() throws RecognitionException {
		try {
			int _type = AND;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:14:4: ( 'AND' | 'and' )
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='A') ) {
				alt2=1;
			}
			else if ( (LA2_0=='a') ) {
				alt2=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 2, 0, input);
				throw nvae;
			}

			switch (alt2) {
				case 1 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:14:4: 'AND'
					{
					match("AND"); 

					}
					break;
				case 2 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:14:12: 'and'
					{
					match("and"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "AND"

	// $ANTLR start "NAME"
	public final void mNAME() throws RecognitionException {
		try {
			int _type = NAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:17:7: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '~' | '!' | '@' | '#' | '$' | '%' | '^' | '&' | '*' | '(' | ')' | '-' | '_' | '=' | '+' | '[' | ']' | '{' | '}' | ',' | '.' )* )
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:17:10: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '~' | '!' | '@' | '#' | '$' | '%' | '^' | '&' | '*' | '(' | ')' | '-' | '_' | '=' | '+' | '[' | ']' | '{' | '}' | ',' | '.' )*
			{
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:17:10: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '~' | '!' | '@' | '#' | '$' | '%' | '^' | '&' | '*' | '(' | ')' | '-' | '_' | '=' | '+' | '[' | ']' | '{' | '}' | ',' | '.' )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0=='!'||(LA3_0 >= '#' && LA3_0 <= '&')||(LA3_0 >= '(' && LA3_0 <= '.')||(LA3_0 >= '0' && LA3_0 <= '9')||LA3_0=='='||(LA3_0 >= '@' && LA3_0 <= '[')||(LA3_0 >= ']' && LA3_0 <= '_')||(LA3_0 >= 'a' && LA3_0 <= '{')||(LA3_0 >= '}' && LA3_0 <= '~')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:
					{
					if ( input.LA(1)=='!'||(input.LA(1) >= '#' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '.')||(input.LA(1) >= '0' && input.LA(1) <= '9')||input.LA(1)=='='||(input.LA(1) >= '@' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '_')||(input.LA(1) >= 'a' && input.LA(1) <= '{')||(input.LA(1) >= '}' && input.LA(1) <= '~') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NAME"

	// $ANTLR start "SPACE"
	public final void mSPACE() throws RecognitionException {
		try {
			int _type = SPACE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:20:4: ( ( ' ' )* )
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:20:4: ( ' ' )*
			{
			// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:20:4: ( ' ' )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:20:5: ' '
					{
					match(' '); 
					}
					break;

				default :
					break loop4;
				}
			}

			skip();
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPACE"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:8: ( T__8 | T__9 | OR | AND | NAME | SPACE )
		int alt5=6;
		switch ( input.LA(1) ) {
		case '(':
			{
			int LA5_1 = input.LA(2);
			if ( (LA5_1=='!'||(LA5_1 >= '#' && LA5_1 <= '&')||(LA5_1 >= '(' && LA5_1 <= '.')||(LA5_1 >= '0' && LA5_1 <= '9')||LA5_1=='='||(LA5_1 >= '@' && LA5_1 <= '[')||(LA5_1 >= ']' && LA5_1 <= '_')||(LA5_1 >= 'a' && LA5_1 <= '{')||(LA5_1 >= '}' && LA5_1 <= '~')) ) {
				alt5=5;
			}

			else {
				alt5=1;
			}

			}
			break;
		case ')':
			{
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='!'||(LA5_2 >= '#' && LA5_2 <= '&')||(LA5_2 >= '(' && LA5_2 <= '.')||(LA5_2 >= '0' && LA5_2 <= '9')||LA5_2=='='||(LA5_2 >= '@' && LA5_2 <= '[')||(LA5_2 >= ']' && LA5_2 <= '_')||(LA5_2 >= 'a' && LA5_2 <= '{')||(LA5_2 >= '}' && LA5_2 <= '~')) ) {
				alt5=5;
			}

			else {
				alt5=2;
			}

			}
			break;
		case 'O':
			{
			int LA5_3 = input.LA(2);
			if ( (LA5_3=='R') ) {
				int LA5_11 = input.LA(3);
				if ( (LA5_11=='!'||(LA5_11 >= '#' && LA5_11 <= '&')||(LA5_11 >= '(' && LA5_11 <= '.')||(LA5_11 >= '0' && LA5_11 <= '9')||LA5_11=='='||(LA5_11 >= '@' && LA5_11 <= '[')||(LA5_11 >= ']' && LA5_11 <= '_')||(LA5_11 >= 'a' && LA5_11 <= '{')||(LA5_11 >= '}' && LA5_11 <= '~')) ) {
					alt5=5;
				}

				else {
					alt5=3;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'o':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='r') ) {
				int LA5_12 = input.LA(3);
				if ( (LA5_12=='!'||(LA5_12 >= '#' && LA5_12 <= '&')||(LA5_12 >= '(' && LA5_12 <= '.')||(LA5_12 >= '0' && LA5_12 <= '9')||LA5_12=='='||(LA5_12 >= '@' && LA5_12 <= '[')||(LA5_12 >= ']' && LA5_12 <= '_')||(LA5_12 >= 'a' && LA5_12 <= '{')||(LA5_12 >= '}' && LA5_12 <= '~')) ) {
					alt5=5;
				}

				else {
					alt5=3;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'A':
			{
			int LA5_5 = input.LA(2);
			if ( (LA5_5=='N') ) {
				int LA5_13 = input.LA(3);
				if ( (LA5_13=='D') ) {
					int LA5_16 = input.LA(4);
					if ( (LA5_16=='!'||(LA5_16 >= '#' && LA5_16 <= '&')||(LA5_16 >= '(' && LA5_16 <= '.')||(LA5_16 >= '0' && LA5_16 <= '9')||LA5_16=='='||(LA5_16 >= '@' && LA5_16 <= '[')||(LA5_16 >= ']' && LA5_16 <= '_')||(LA5_16 >= 'a' && LA5_16 <= '{')||(LA5_16 >= '}' && LA5_16 <= '~')) ) {
						alt5=5;
					}

					else {
						alt5=4;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'a':
			{
			int LA5_6 = input.LA(2);
			if ( (LA5_6=='n') ) {
				int LA5_14 = input.LA(3);
				if ( (LA5_14=='d') ) {
					int LA5_17 = input.LA(4);
					if ( (LA5_17=='!'||(LA5_17 >= '#' && LA5_17 <= '&')||(LA5_17 >= '(' && LA5_17 <= '.')||(LA5_17 >= '0' && LA5_17 <= '9')||LA5_17=='='||(LA5_17 >= '@' && LA5_17 <= '[')||(LA5_17 >= ']' && LA5_17 <= '_')||(LA5_17 >= 'a' && LA5_17 <= '{')||(LA5_17 >= '}' && LA5_17 <= '~')) ) {
						alt5=5;
					}

					else {
						alt5=4;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case ' ':
			{
			alt5=6;
			}
			break;
		default:
			alt5=5;
		}
		switch (alt5) {
			case 1 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:10: T__8
				{
				mT__8(); 

				}
				break;
			case 2 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:15: T__9
				{
				mT__9(); 

				}
				break;
			case 3 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:20: OR
				{
				mOR(); 

				}
				break;
			case 4 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:23: AND
				{
				mAND(); 

				}
				break;
			case 5 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:27: NAME
				{
				mNAME(); 

				}
				break;
			case 6 :
				// C:\\Users\\vbc\\git\\MobiloQE\\MobiloQueryEngine\\src\\com\\mobiloqueryengine\\resources\\MobiloQueryGrammer.g:1:32: SPACE
				{
				mSPACE(); 

				}
				break;

		}
	}



}
