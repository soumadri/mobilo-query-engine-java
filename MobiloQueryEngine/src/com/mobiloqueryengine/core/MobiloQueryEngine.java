package com.mobiloqueryengine.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ResourceBundle;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.apache.log4j.Logger;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.PostingListReference;
import com.mobiloindexer.domain.SearchIndex;
import com.mobiloindexer.domain.SearchResult;

/**
 * Query engine for Mobilo index
 * @author Soumadri Roy
 *
 */
public class MobiloQueryEngine {

	ISearchResultEvaluator searchResultEvaluator;
	private final String PATH_SEPERATOR = System.getProperty("file.separator");
	
	private final static ResourceBundle miResourceBundle = ResourceBundle.getBundle("com.mobiloqueryengine.resources.mobiloqueryengine");
	private final static Logger LOGGER = Logger.getLogger(MobiloQueryEngine.class);
	
	public MobiloQueryEngine(String pathToIndex) throws IOException{
		LOGGER.info("Initializing query engine");
		
		String path = pathToIndex.endsWith(PATH_SEPERATOR) ? pathToIndex : pathToIndex + PATH_SEPERATOR;
		
		//Initialize the index
		String indexFileName = miResourceBundle.getString("index.entrypoint") + miResourceBundle.getString("index.extn");
		SearchIndex index = new SearchIndex(path, (HashMap<String,PostingListReference>) IndexUtils.readIndex(path + indexFileName));
		
		//Initialize the result evaluator with the index
		searchResultEvaluator = new SearchResultEvaluatorImpl(index);	
		
		LOGGER.info("Query engine initialized");
	}
	
	public SearchResult search(String query) throws ClassNotFoundException, RecognitionException, IOException{
		LinkedList<DocumentReference> result = evaluateQuery(query);
		
		if(null == result)
			return null;
		else {
			SearchResult searchResult = new SearchResult(result);
			return searchResult;
		}
	}
	
	/**
	 * Evaluates the search query by building and AST and traversing it recursively and evaluating based on the boolean operator 
	 * @param query The plaintext query
	 * @return A list of document references where the query has matched
	 * @throws RecognitionException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private LinkedList<DocumentReference> evaluateQuery(String query)
			throws RecognitionException, IOException, ClassNotFoundException {
		ANTLRStringStream input = new ANTLRStringStream(query);
		TokenStream tokens = new CommonTokenStream(new MobiloQueryGrammerLexer(
				input));

		//Parser generates abstract syntax tree
		MobiloQueryGrammerParser parser = new MobiloQueryGrammerParser(tokens);
		MobiloQueryGrammerParser.start_return ret = parser.start();

		//Acquire parse result
		CommonTree ast = (CommonTree) ret.tree;
		return evaluateTree(ast);

	}

	private LinkedList<DocumentReference> evaluateTree(CommonTree ast) throws IOException, ClassNotFoundException {
		//Recursively descend the parse tree
		if (ast.getChildren() != null) {
			LinkedList<DocumentReference> left = evaluateTree((CommonTree) ast
					.getChild(0));
			LinkedList<DocumentReference> right = evaluateTree((CommonTree) ast
					.getChild(1));			
			return evaluate(ast.getText(), left, right);
		} else {
			//System.out.println("Getting search result for "+ast.getText()+" result size="+searchResultEvaluator.getSearchResults(ast.getText()).size());
			LOGGER.debug("Evaluating keyword: "+ast.getText());
			return searchResultEvaluator.getSearchResults(ast.getText());
		}

	}

	private LinkedList<DocumentReference> evaluate(String operator,
			LinkedList<DocumentReference> left,
			LinkedList<DocumentReference> right) {
		
		LinkedList<DocumentReference> result=null;
		
		if(operator.equalsIgnoreCase(miResourceBundle.getString("OR-OPERATOR"))){
			System.out.println("ORing...");
			result= searchResultEvaluator.unionResults(left, right);	//OR operator does union
		}
		else if(operator.equalsIgnoreCase(miResourceBundle.getString("AND-OPERATOR"))){
			System.out.println("ANDing...");
			result= searchResultEvaluator.intersectResults(left, right);	//AND operator does intersection
		}
		return result;
	}

}
