package com.mobiloqueryengine.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;

/**
 * This class has several utility methods for the indexer
 * @author Soumadri Roy
 *
 */
public final class IndexUtils {
	/**
	 * Deletes all temporary forward indexes for which the filename starts with '~'
	 * @param outputPath Output directory pathwhere the index is being generated
	 */
	public static void deleteTempIndexes(String outputPath){
		File folder = new File(outputPath);
		for (File fileEntry : folder.listFiles()){
			//Any file starts with '~' is temp indexes
			if(fileEntry.getName().startsWith("~"))
				fileEntry.delete();
		}
	}
	
	/**
	 * Generates an integer hash for a given input string
	 * @param x	The input string
	 * @return	The has of the string
	 */
	public static Integer hash(String x) {
		int hash=7;
		for (int i=0; i < x.length(); i++) {
		    hash = hash*31 + x.charAt(i);
		}
		return new Integer(hash);
	}
	
	/**
	 * Generates an integer hash for a given input string
	 * @param x	The input string
	 * @return	The has of the string
	 */
	public static int hashInt(String x) {
		int hash=7;
		for (int i=0; i < x.length(); i++) {
		    hash = hash*31 + x.charAt(i);
		}
		return hash;
	}
	
	/**
	 * Serializes the specified object to disk using Hessian serializer
	 * @param data	The object to be serialized
	 * @param fileName	The name of the file to write to
	 * @throws IOException
	 */
	public static void writeIndex(Object data, String fileName) throws IOException {
		OutputStream os = new FileOutputStream(fileName);
		Hessian2Output out = new Hessian2Output(os);
		out.writeObject(data);
		out.close();
		os.close();		
	}
	
	/**
	 * Reads a serialized index from file
	 * @param fileName	The name of the file in which the object is serialized
	 * @return	The deserialized object
	 * @throws IOException
	 */
	public static Object readIndex(String fileName) throws IOException {
		FileInputStream is = new FileInputStream(fileName);
		Hessian2Input in = new Hessian2Input(is);
		Object obj = in.readObject();
		in.close();
		is.close();		
		
		return obj;		
	}
}
