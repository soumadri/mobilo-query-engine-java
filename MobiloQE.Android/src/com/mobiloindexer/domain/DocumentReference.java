package com.mobiloindexer.domain;

import java.io.Serializable;
import java.util.LinkedList;

public class DocumentReference implements Serializable, Comparable<DocumentReference>{
	
	//String docid;
	int docid;
	String docName;	
	int hitcount;
	LinkedList<Hit> hitlist;
	
	
	public int getHitcount() {
		return hitcount;
	}
	public void setHitcount(int hitcount) {
		this.hitcount = hitcount;
	}
	public int getDocid() {
		return docid;
	}
	public void setDocid(int docid) {
		this.docid = docid;
	}
	public LinkedList<Hit> getHitlist() {
		return hitlist;
	}
	public void setHitlist(LinkedList<Hit> hitlist) {
		this.hitlist = hitlist;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	
	@Override
	public int compareTo(DocumentReference o) {
		int cDocId = o.getDocid();
		
		if (this.docid > cDocId) {
			return 1;
		} else if (this.docid == cDocId) {
			return 0;
		} else {
			return -1;
		}		
	}
 
}
