package com.mobiloindexer.domain;

import java.util.HashMap;

/**
 * Represents a Search index 
 * @author Soumadri Roy
 *
 */
public class SearchIndex {
	String path;
	HashMap<String,PostingListReference> index;
	
	public SearchIndex(String path, HashMap<String,PostingListReference> index){
		this.path = path;
		this.index = index;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public HashMap<String, PostingListReference> getIndex() {
		return index;
	}
	public void setIndex(HashMap<String, PostingListReference> index) {
		this.index = index;
	}
}
