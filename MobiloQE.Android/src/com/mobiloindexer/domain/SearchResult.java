package com.mobiloindexer.domain;

import java.util.LinkedList;

public class SearchResult {
			
	private long matchCount = 0;
	private long matchedDocumentsCount = 0;
	private LinkedList<DocumentReference> documentReferences;
	
	public SearchResult(LinkedList<DocumentReference> documentReferences){
		this.documentReferences = documentReferences;
		
		matchedDocumentsCount = documentReferences.size();
				
		for (DocumentReference documentReference : documentReferences) {
			matchCount += documentReference.getHitcount();
		}
			
	}
	
	public long getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(long matchCount) {
		this.matchCount = matchCount;
	}
	public long getMatchedDocumentsCount() {
		return matchedDocumentsCount;
	}
	public void setMatchedDocumentsCount(long matchedDocumentsCount) {
		this.matchedDocumentsCount = matchedDocumentsCount;
	}
	public LinkedList<DocumentReference> getDocumentReferences() {
		return documentReferences;
	}
	public void setDocumentReferences(
			LinkedList<DocumentReference> documentReferences) {
		this.documentReferences = documentReferences;
	}
}
