package com.mobiloindexer.domain;
import java.io.Serializable;


public class Hit implements Serializable {
	int startposition;
	int endposition;
	public int getStartposition() {
		return startposition;
	}
	public void setStartposition(int startposition) {
		this.startposition = startposition;
	}
	public int getEndposition() {
		return endposition;
	}
	public void setEndposition(int endposition) {
		this.endposition = endposition;
	}
	

}
