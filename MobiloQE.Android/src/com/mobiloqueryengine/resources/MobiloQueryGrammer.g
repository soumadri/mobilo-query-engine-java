grammar MobiloQueryGrammer;

options {
  output=AST;
  ASTLabelType=CommonTree;
    }
    
    
    OR    
:	 'OR' | 'or'
;

AND   
:	 'AND' | 'and'
;

NAME 	: 	('a'..'z' | 'A'..'Z' | '0'..'9' | '~' | '!' | '@' | '#' | '$' | '%' | '^' | '&' | '*' | '(' | ')' | ',' | '.')*;

SPACE 
: 	(' ')* {skip();}
;

start 
 : andExpr
 ;

andExpr
 : (orExpr) ((AND^ orExpr)*)
 ;


orExpr
 : (atom) ((OR^ atom)*)
 ; 

atom
 : 	'('! andExpr ')'!
 | NAME
 ;
 
 

  
