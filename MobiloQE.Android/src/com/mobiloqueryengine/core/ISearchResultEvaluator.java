package com.mobiloqueryengine.core;

import java.io.IOException;
import java.util.LinkedList;

import com.mobiloindexer.domain.DocumentReference;

public interface ISearchResultEvaluator {
	
	/**
	 * Retrieves the search result for a single token in the input search query
	 * @param keyword	The single token keyword from the query
	 * @return	A list of document references where the keyword has matched
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public LinkedList<DocumentReference> getSearchResults(String keyword) throws IOException, ClassNotFoundException;
	
	/**
	 * Intersects or performs ANDing of search criteria
	 * @param resultSet1
	 * @param resultSet2
	 * @return	An intersected list of document references where the keyword has matched
	 */
	public LinkedList<DocumentReference> intersectResults(LinkedList<DocumentReference> resultSet1, LinkedList<DocumentReference> resultSet2);
	
	/**
	 * Unions or performs ORing of search criteria
	 * @param resultSet1
	 * @param resultSet2
	 * @return An unioned list of document references where the keyword has matched
	 */
	public LinkedList<DocumentReference> unionResults(LinkedList<DocumentReference> resultSet1, LinkedList<DocumentReference> resultSet2);
	
	//public LinkedList<DocumentReference> finalize(LinkedList<DocumentReference> resultSet1);
	
}
