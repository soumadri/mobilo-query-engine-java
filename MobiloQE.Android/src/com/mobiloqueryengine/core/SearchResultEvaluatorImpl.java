package com.mobiloqueryengine.core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import android.util.Log;

import com.mobiloindexer.domain.DocumentReference;
import com.mobiloindexer.domain.PostingListReference;
import com.mobiloindexer.domain.SearchIndex;
import com.mobiloindexer.processes.Stemmer;

/**
 * The evaluator for the search results
 * @author Soumadri Roy
 *
 */
public class SearchResultEvaluatorImpl implements ISearchResultEvaluator{
	SearchIndex searchIndex;
	
	//private final static Logger LOGGER = Logger.getLogger(SearchResultEvaluatorImpl.class);
	
	public SearchResultEvaluatorImpl(SearchIndex searchIndex){
		this.searchIndex = searchIndex;
	}

	@Override
	public LinkedList<DocumentReference> getSearchResults(String keyword) throws IOException, ClassNotFoundException {
		/**
		 * stemming the search keyword
		 */
		String kword = keyword.toLowerCase();
		Stemmer stemmer = new Stemmer();
		stemmer.add(kword);
		stemmer.stem();
		String stemmedKeyword = stemmer.toString();
		
		Log.i("MQE","Stemmed Keyword: "+stemmedKeyword);
		
		//Get the Posting list reference which is used to get the doclist from the fragmented index		
		PostingListReference postingListReference = searchIndex.getIndex().get(IndexUtils.hash(stemmedKeyword));
		
		if(null != postingListReference){
			HashMap<String, LinkedList<DocumentReference>> block = (HashMap<String, LinkedList<DocumentReference>>) IndexUtils.readIndex(searchIndex.getPath() + postingListReference.getPostingblockid() + ".indx");
	
			//Getting the doc list for the search keyword		
			LinkedList<DocumentReference> matchedDocList = block.get(postingListReference.getPostinglistid());
						
			Log.i("MQE","Results retrived");
			
			return matchedDocList;
			
		}else{
			return null;
		}
		
		
	}

	@Override
	public LinkedList<DocumentReference> intersectResults(
			LinkedList<DocumentReference> resultSet1,
			LinkedList<DocumentReference> resultSet2) {
		
		if(null==resultSet1 && null==resultSet2)
			return null;
		else if(null==resultSet1)	//If set 1 is empty
			return resultSet2;	//then return set 2
		else if(null==resultSet2)
			return resultSet1;	//otherwise return set1						
		
		return merge(resultSet1, resultSet2);
	}
	
	private LinkedList<DocumentReference> merge(LinkedList<DocumentReference> resultSet1, LinkedList<DocumentReference> resultSet2){
		Iterator<DocumentReference> iterator1 =  resultSet1.iterator();
		Iterator<DocumentReference> iterator2 =  resultSet2.iterator();
		
		LinkedList<DocumentReference> mergedList = new LinkedList<DocumentReference>();
		
		while(iterator1.hasNext() && iterator2.hasNext()){
			DocumentReference dr1 = iterator1.next();
			DocumentReference dr2 = iterator2.next();
			
			if(dr1.getDocid() == dr2.getDocid()){
				//Merge the hitlist and count
				dr1.setHitcount(dr1.getHitcount() + dr2.getHitcount());		//Accumulate the hit counts
				dr1.getHitlist().addAll(dr2.getHitlist());	//Add all the hits
				
				mergedList.add(dr1);	//Add it to the final list
			} else if(dr1.getDocid() < dr2.getDocid()){
				iterator1.next();
			} else{
				iterator2.next();
			}
						
		}
		
		return mergedList;
	}

	@Override
	public LinkedList<DocumentReference> unionResults(
			LinkedList<DocumentReference> resultSet1,
			LinkedList<DocumentReference> resultSet2) {
		
		if(null==resultSet1 && null==resultSet2)
			return null;
		else if(null==resultSet1)	//If set 1 is empty
			return resultSet2;	//then return set 2
		else if(null==resultSet2)
			return resultSet1;	//otherwise return set1
		
		LinkedList<DocumentReference> unionList = new LinkedList<DocumentReference>();
		Iterator<DocumentReference> iterator =  resultSet2.iterator();
		
		while(iterator.hasNext()){
			DocumentReference currItem = iterator.next();
			if(!resultSet1.contains(currItem)){
				unionList.add(currItem);
			}
			
		}
		
		Iterator<DocumentReference> unionIterator = unionList.iterator();
		while(unionIterator.hasNext()){
			resultSet1.add(unionIterator.next());
			
		}
		
		return resultSet1;
	}

	/*@Override
	public LinkedList<DocumentReference> finalize(
			LinkedList<DocumentReference> resultSet1) {
		
		
		LinkedList<DocumentReference> sortedList = new LinkedList<DocumentReference>();
		
		Boolean initSwitch = true;
		for(DocumentReference curRef:resultSet1){
			if(initSwitch){
				sortedList.add(curRef);
				
				initSwitch = false;
			}else{
				
				insert(sortedList, curRef);
				
				
			}
		}
		
		return sortedList;
	}
	
	public void insert(LinkedList<DocumentReference> sortList, DocumentReference DocumentReference){
		int index = 0;
		int listSize = sortList.size(); 
		while(true){
			
			if(sortList.get(index).getHitcount() > DocumentReference.getHitcount()){
				if((index == (listSize-1)) || (sortList.get(index+1).getHitcount() <= DocumentReference.getHitcount())){
					sortList.add((index+1), DocumentReference);
					break;
				}
			}else{
				sortList.add(index, DocumentReference);
				break;
			}	
			
			index++;
		}
		
		
	}*/
}
